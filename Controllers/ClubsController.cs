using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Espit.Data;
using Espit.Models;
using Espit.Models.Entries;
using Espit.Models.Wrappers;
using Espit.Services;

namespace Espit.Controllers;

public class ClubsController(EspitContext context) : Controller
{
    public async Task<IActionResult> Index()
    {
        // Get all registered Clubs
        var registeredClubs = context.Clubs.ToListAsync().Result;
        var group = new UserGroup(context);
        var thisUserGroup = group.GetUserGroup(User.Claims);

        var wrapper = new List<ClubsWrapper>();
        foreach (var club in registeredClubs)
        {
            if (!new Eligibility().IsEligibleFor(thisUserGroup, club) && !group.IsTeacher(User.Claims))
            {
                continue;
            }

            var thisClubWrapper = new ClubsWrapper
            {
                Club = club,
                EnrolledStudents = context.StudentClubEntries.Count(e => e.ClubId == club.Id)
            };

            if (group.IsTeacher(User.Claims))
            {
                thisClubWrapper.HasElevatedPrivileges =
                    thisClubWrapper.HasElevatedPrivileges with { isTeacher = true };
            }
            else
            {
                thisClubWrapper.StudentWrapper = new StudentClubsController(context)
                    .GetStudentClubsWrapper(User.Claims.FirstOrDefault(c => c.Type == "name")!.Value);
            }

            if (group.IsDev(User.Claims))
            {
                thisClubWrapper.HasElevatedPrivileges =
                    thisClubWrapper.HasElevatedPrivileges with { isDev = true };
            }

            wrapper.Add(thisClubWrapper);
        }

        return View(wrapper);
    }

    // GET: Clubs/Create
    public IActionResult Create()
    {
        // Disallow access for unauthenticated users and non-privileged users
        var uGroup = new UserGroup(context);
        if (User.Identity == null || (!uGroup.IsTeacher(User.Claims) && !uGroup.IsDev(User.Claims)))
        {
            return RedirectToAction(nameof(Index));
        }

        return View();
    }

    // POST: Workshop/Create
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Create(
        [Bind(
            "Id,Title,EligibleClassSecunda,EligibleClassTercie,EligibleClassQuarta,EligibleClassQuinta,EligibleClassSexta,EligibleClassSeptima,EligibleClassOctava,EligibleClassSophomore,EligibleClassJunior,EligibleClassSenior")]
        Club club)
    {
        // Disallow access for unauthenticated users and non-privileged users
        var uGroup = new UserGroup(context);
        if (User.Identity == null || (!uGroup.IsTeacher(User.Claims) && !uGroup.IsDev(User.Claims)))
        {
            return RedirectToAction(nameof(Index));
        }

        if (ModelState.IsValid)
        {
            context.Add(club);
            await context.SaveChangesAsync();
            //return RedirectToAction(nameof(Index));
            return RedirectToAction(nameof(Create));
        }

        return View(club);
    }

    public async Task<IActionResult> Edit(int? id)
    {
        // Disallow access for unauthenticated users and non-privileged users
        var uGroup = new UserGroup(context);
        if (User.Identity == null || (!uGroup.IsTeacher(User.Claims) && !uGroup.IsDev(User.Claims)))
        {
            return RedirectToAction(nameof(Index));
        }

        if (id == null)
        {
            return NotFound();
        }

        var Club = await context.Clubs.FindAsync(id);
        if (Club == null)
        {
            return NotFound();
        }

        return View(Club);
    }

    // POST: Workshop/Edit/5
    // To protect from overposting attacks, enable the specific properties you want to bind to.
    // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Edit(int id,
        [Bind(
            "Id,Title,EligibleClassSecunda,EligibleClassTercie,EligibleClassQuarta,EligibleClassQuinta,EligibleClassSexta,EligibleClassSeptima,EligibleClassOctava,EligibleClassSophomore,EligibleClassJunior,EligibleClassSenior")]
        Club club)
    {
        // Disallow access for unauthenticated users and non-privileged users
        var uGroup = new UserGroup(context);
        if (User.Identity == null || (!uGroup.IsTeacher(User.Claims) && !uGroup.IsDev(User.Claims)))
        {
            return RedirectToAction(nameof(Index));
        }

        if (id != club.Id)
        {
            return NotFound();
        }

        if (ModelState.IsValid)
        {
            try
            {
                context.Update(club);
                await context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ClubExists(club.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToAction(nameof(Index));
        }

        return View(club);
    }

    private bool ClubExists(int id)
    {
        return context.Clubs.Any(e => e.Id == id);
    }

    public async Task<IActionResult> Register(int ClubId, bool mandatoryChoice)
    {
        // Disallow registration for teachers and unauthenticated users
        var uGroup = new UserGroup(context);
        if (User.Identity == null || uGroup.IsTeacher(User.Claims))
        {
            return RedirectToAction(nameof(Index));
        }

        // Get the club and check if it exists
        var Club = await context.Clubs.FindAsync(ClubId);
        if (Club == null)
        {
            return RedirectToAction(nameof(Index));
        }

        var userName = User.Claims.FirstOrDefault(c => c.Type == "name")!.Value;
        var thisStudentEntries = context.StudentClubEntries.Where(e => e.Name == userName);

        // Check if user is already registered for this club
        foreach (var studentEntry in thisStudentEntries)
        {
            if(studentEntry.ClubId == ClubId)
            {
                return RedirectToAction(nameof(Index));
            }
        }

        // TO BE REMOVED
        // Check if the club is full
        if (Club.Title == "Právo v praxi")
        {
            var count = context.StudentClubEntries.Count(e => e.ClubId == ClubId);
            if (count >= 20)
            {
                return RedirectToAction(nameof(Index));
            }
        }
        
        context.Add(new StudentClubEntry()
        {
            Name = new Name().SwapFirstAndLastName(userName),
            Class = new UserGroup(context).GetUserGroup(User.Claims),
            ClubId = ClubId,
        });
        
        await context.SaveChangesAsync();
        return RedirectToAction(nameof(Index));
    }

    public async Task<IActionResult> Deregister(int ClubId)
    {
        // Disallow deregistration for teachers and unauthenticated users
        var uGroup = new UserGroup(context);
        if (User.Identity == null || uGroup.IsTeacher(User.Claims))
        {
            return RedirectToAction(nameof(Index));
        }

        // Check if the currently registered club exists
        var Club = await context.Clubs.FindAsync(ClubId);
        if (Club == null)
        {
            return RedirectToAction(nameof(Index));
        }

        var userName = User.Claims.FirstOrDefault(c => c.Type == "name")!.Value;
        var thisStudentEntries = context.StudentClubEntries.Where(e => e.Name == userName);
        
        // Check if user is already registered for this club
        foreach (var studentEntry in thisStudentEntries)
        {
            if(studentEntry.ClubId == ClubId)
            {
                context.Remove(studentEntry);
                await context.SaveChangesAsync();
            }
        }

        return RedirectToAction(nameof(Index));
    }
    
    /*Class*/
}