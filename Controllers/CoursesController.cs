using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Espit.Data;
using Espit.Models;
using Espit.Models.Entries;
using Espit.Models.Wrappers;
using Espit.Services;

namespace Espit.Controllers;

public class CoursesController(EspitContext context) : Controller
{
    public async Task<IActionResult> Index()
    {
        // Get all registered courses
        var registeredCourses = context.Courses.ToListAsync().Result;
        var group = new UserGroup(context);
        var thisUserGroup = group.GetUserGroup(User.Claims);

        var wrapper = new List<CoursesWrapper>();
        foreach (var course in registeredCourses)
        {
            if (!new Eligibility().IsEligibleFor(thisUserGroup, course) && !group.IsTeacher(User.Claims))
            {
                continue;
            }

            var thisCourseWrapper = new CoursesWrapper
            {
                Course = course,
                EnrolledStudents = context.StudentCourseEntries.Count(e => e.CourseId == course.Id)
            };

            if (group.IsTeacher(User.Claims))
            {
                thisCourseWrapper.HasElevatedPrivileges =
                    thisCourseWrapper.HasElevatedPrivileges with { isTeacher = true };
            }
            else
            {
                thisCourseWrapper.StudentWrapper = new StudentCoursesController(context)
                    .GetStudentCoursesWrapper(User.Claims.FirstOrDefault(c => c.Type == "name")!.Value);
            }

            if (group.IsDev(User.Claims))
            {
                thisCourseWrapper.HasElevatedPrivileges =
                    thisCourseWrapper.HasElevatedPrivileges with { isDev = true };
            }

            
            
            thisCourseWrapper.StudentWrapper.ForcedCourseCount = thisUserGroup switch
            {
                "6.V" or "2.B" => 4,
                "7.V" or "3.B" => 3,
                "1.V" or "3.V" or "4.V" => 1,
                _ => thisCourseWrapper.StudentWrapper.ForcedCourseCount
            };

            wrapper.Add(thisCourseWrapper);
        }

        return View(wrapper);
    }

    // GET: Courses/Create
    public IActionResult Create()
    {
        // Disallow access for unauthenticated users and non-privileged users
        var uGroup = new UserGroup(context);
        if (User.Identity == null || (!uGroup.IsTeacher(User.Claims) && !uGroup.IsDev(User.Claims)))
        {
            return RedirectToAction(nameof(Index));
        }

        return View();
    }

    // POST: Workshop/Create
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Create(
        [Bind(
            "Id,Title,EligibleClassSecunda,EligibleClassTercie,EligibleClassQuarta,EligibleClassQuinta,EligibleClassSexta,EligibleClassSeptima,EligibleClassOctava,EligibleClassSophomore,EligibleClassJunior,EligibleClassSenior")]
        Course course)
    {
        // Disallow access for unauthenticated users and non-privileged users
        var uGroup = new UserGroup(context);
        if (User.Identity == null || (!uGroup.IsTeacher(User.Claims) && !uGroup.IsDev(User.Claims)))
        {
            return RedirectToAction(nameof(Index));
        }

        if (ModelState.IsValid)
        {
            context.Add(course);
            await context.SaveChangesAsync();
            //return RedirectToAction(nameof(Index));
            return RedirectToAction(nameof(Create));
        }

        return View(course);
    }

    public async Task<IActionResult> Edit(int? id)
    {
        // Disallow access for unauthenticated users and non-privileged users
        var uGroup = new UserGroup(context);
        if (User.Identity == null || (!uGroup.IsTeacher(User.Claims) && !uGroup.IsDev(User.Claims)))
        {
            return RedirectToAction(nameof(Index));
        }

        if (id == null)
        {
            return NotFound();
        }

        var course = await context.Courses.FindAsync(id);
        if (course == null)
        {
            return NotFound();
        }

        return View(course);
    }

    // POST: Workshop/Edit/5
    // To protect from overposting attacks, enable the specific properties you want to bind to.
    // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Edit(int id,
        [Bind(
            "Id,Title,EligibleClassSecunda,EligibleClassTercie,EligibleClassQuarta,EligibleClassQuinta,EligibleClassSexta,EligibleClassSeptima,EligibleClassOctava,EligibleClassSophomore,EligibleClassJunior,EligibleClassSenior")]
        Course course)
    {
        // Disallow access for unauthenticated users and non-privileged users
        var uGroup = new UserGroup(context);
        if (User.Identity == null || (!uGroup.IsTeacher(User.Claims) && !uGroup.IsDev(User.Claims)))
        {
            return RedirectToAction(nameof(Index));
        }

        if (id != course.Id)
        {
            return NotFound();
        }

        if (ModelState.IsValid)
        {
            try
            {
                context.Update(course);
                await context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CourseExists(course.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToAction(nameof(Index));
        }

        return View(course);
    }

    private bool CourseExists(int id)
    {
        return context.Courses.Any(e => e.Id == id);
    }

    public async Task<IActionResult> Register(int courseId, bool mandatoryChoice)
    {
        // Disallow registration for teachers and unauthenticated users
        var uGroup = new UserGroup(context);
        if (User.Identity == null || uGroup.IsTeacher(User.Claims))
        {
            return RedirectToAction(nameof(Index));
        }

        // Get the course and check if it exists
        var course = await context.Courses.FindAsync(courseId);
        if (course == null)
        {
            return RedirectToAction(nameof(Index));
        }

        var userName = User.Claims.FirstOrDefault(c => c.Type == "name")!.Value;
        var thisStudentEntries = context.StudentCourseEntries.Where(e => e.Name == userName);

        // Check if user is already registered for this course
        foreach (var studentEntry in thisStudentEntries)
        {
            if (studentEntry.CourseId == courseId)
            {
                return RedirectToAction(nameof(Index));
            }
        }

        // TO BE REMOVED
        // Check if the course is full
        if (course.Title == "Právo v praxi")
        {
            var count = context.StudentCourseEntries.Count(e => e.CourseId == courseId);
            if (count >= 20)
            {
                return RedirectToAction(nameof(Index));
            }
        }

        context.Add(new StudentCourseEntry()
        {
            Name = new Name().SwapFirstAndLastName(userName),
            Class = new UserGroup(context).GetUserGroup(User.Claims),
            CourseId = courseId,
            Mandatory = mandatoryChoice
        });

        await context.SaveChangesAsync();
        return RedirectToAction(nameof(Index));
    }

    public async Task<IActionResult> Deregister(int courseId)
    {
        // Disallow deregistration for teachers and unauthenticated users
        var uGroup = new UserGroup(context);
        if (User.Identity == null || uGroup.IsTeacher(User.Claims))
        {
            return RedirectToAction(nameof(Index));
        }

        // Check if the currently registered course exists
        var course = await context.Courses.FindAsync(courseId);
        if (course == null)
        {
            return RedirectToAction(nameof(Index));
        }

        var userName = User.Claims.FirstOrDefault(c => c.Type == "name")!.Value;
        var thisStudentEntries = context.StudentCourseEntries.Where(e => e.Name == userName);

        // Check if user is already registered for this course
        foreach (var studentEntry in thisStudentEntries)
        {
            if (studentEntry.CourseId == courseId)
            {
                context.Remove(studentEntry);
                await context.SaveChangesAsync();
            }
        }

        return RedirectToAction(nameof(Index));
    }

    /*Class*/
}