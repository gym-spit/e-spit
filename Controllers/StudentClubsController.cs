using Espit.Data;
using Espit.Models;
using Espit.Models.Entries;
using Espit.Models.Wrappers;
using Espit.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Espit.Controllers;

public class StudentClubsController(EspitContext context) : Controller
{
    public async Task<IActionResult> Index()
    {
        // Disallow access for unauthenticated users and non-privileged users
        var uGroup = new UserGroup(context);
        if (User.Identity == null || (!uGroup.IsTeacher(User.Claims) && !uGroup.IsDev(User.Claims)))
        {
            return NotFound();
        }
            
        var studentEntries = context.StudentWorkshopEntries.ToListAsync().Result;
        List<StudentWorkshopWrapper> wrappers = new List<StudentWorkshopWrapper>();

        while (studentEntries.Count != 0)
        {
            var entry = studentEntries.First();

            var thisStudentWorkshopEntries = studentEntries.Where(e => e.Name == entry.Name).ToList();
            // var thisStudentWrapper = GetStudentWorkshopWrapper(studentEntries, entry, entry.Name);

            var thisStudentWrapper = new StudentWorkshopWrapper
            {
                Name = entry.Name,
                Class = entry.Class,
                Workshops = new Dictionary<int, Workshop>()
            };
            foreach (var studentWorkshopEntry in thisStudentWorkshopEntries)
            {
                var wsEntry = context.WorkshopEntries
                    .FirstOrDefault(w => w.Id == studentWorkshopEntry.WorkshopEntryId);
                var blockNumber = wsEntry.BlockNumber;
                var ws = context.Workshops.FirstOrDefault(w => w.Id == wsEntry.WorkshopId);

                if (ws == null)
                {
                    throw new Exception();
                }

                thisStudentWrapper.Workshops.Add(blockNumber, ws);
            }

            wrappers.Add(thisStudentWrapper);

            foreach (var e in thisStudentWorkshopEntries)
            {
                studentEntries.Remove(e);
            }
        }

        return View(wrappers);
    }

    
    // Used in the ClubsController Index method for better UX during registration
    public StudentClubsWrapper GetStudentClubsWrapper(string name)
    {
        var studentEntries = context.StudentClubEntries.Where(e => e.Name == name).ToList();

        var studentWrapper = new StudentClubsWrapper
        {
            Name = name,
            Clubs = new List<Club>()
        };

        foreach (var entry in studentEntries)
        {
            var club = context.Clubs.FirstOrDefault(w => w.Id == entry.ClubId);
            if(club == null)
            {
                continue;
            }
            
            studentWrapper.Clubs.Add(club);
        }

        return studentWrapper;
    }
}