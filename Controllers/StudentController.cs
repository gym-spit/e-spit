using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Espit.Data;
using Espit.Models;
using Espit.Services;
using Espit.Models.Wrappers;

namespace Espit.Controllers
{
    public class StudentController(EspitContext context) : Controller
    {
        public async Task<IActionResult> Index()
        {
            // Disallow access for unauthenticated users and non-privileged users
            var uGroup = new UserGroup(context);
            if (User.Identity == null || (!uGroup.IsTeacher(User.Claims) && !uGroup.IsDev(User.Claims)))
            {
                return NotFound();
            }
            
            var studentEntries = context.StudentWorkshopEntries.ToListAsync().Result;
            List<StudentWorkshopWrapper> wrappers = new List<StudentWorkshopWrapper>();

            while (studentEntries.Count != 0)
            {
                var entry = studentEntries.First();

                var thisStudentWorkshopEntries = studentEntries.Where(e => e.Name == entry.Name).ToList();
                // var thisStudentWrapper = GetStudentWorkshopWrapper(studentEntries, entry, entry.Name);

                var thisStudentWrapper = new StudentWorkshopWrapper
                {
                    Name = entry.Name,
                    Class = entry.Class,
                    Workshops = new Dictionary<int, Workshop>()
                };
                foreach (var studentWorkshopEntry in thisStudentWorkshopEntries)
                {
                    var wsEntry = context.WorkshopEntries
                        .FirstOrDefault(w => w.Id == studentWorkshopEntry.WorkshopEntryId);
                    var blockNumber = wsEntry.BlockNumber;
                    var ws = context.Workshops.FirstOrDefault(w => w.Id == wsEntry.WorkshopId);

                    if (ws == null)
                    {
                        throw new Exception();
                    }

                    thisStudentWrapper.Workshops.Add(blockNumber, ws);
                }

                wrappers.Add(thisStudentWrapper);

                foreach (var e in thisStudentWorkshopEntries)
                {
                    studentEntries.Remove(e);
                }
            }

            return View(wrappers);
        }

        // Used in the WorkshopController Index method for better UX during registration
        public StudentWorkshopWrapper GetStudentWorkshopWrapper(string name)
        {
            var studentEntries = context.StudentWorkshopEntries.ToListAsync().Result;
            var thisStudentWorkshopEntries = studentEntries.Where(e => e.Name == name).ToList();

            var thisStudentWrapper = new StudentWorkshopWrapper
            {
                Name = name,
                Workshops = new Dictionary<int, Workshop>()
            };

            foreach (var studentWorkshopEntry in thisStudentWorkshopEntries)
            {
                var wsEntry = context.WorkshopEntries
                    .FirstOrDefault(w => w.Id == studentWorkshopEntry.WorkshopEntryId);
                var blockNumber = wsEntry.BlockNumber;
                var ws = context.Workshops.FirstOrDefault(w => w.Id == wsEntry.WorkshopId);
                thisStudentWrapper.Workshops.Add(blockNumber, ws);
            }

            return thisStudentWrapper;
        }

        public async Task<IActionResult> Entries()
        {
            // Disallow access for unauthenticated users and non-privileged users
            var uGroup = new UserGroup(context);
            if (User.Identity == null || !uGroup.IsTeacher(User.Claims) || !uGroup.IsDev(User.Claims))
            {
                return RedirectToAction(nameof(Index));
            }
            
            return View(await context.StudentWorkshopEntries.ToListAsync());
        }


        public async Task<IActionResult> DeleteEntry(int? id)
        {
            // Disallow access for unauthenticated users and non-privileged users
            var uGroup = new UserGroup(context);
            if (User.Identity == null || !uGroup.IsTeacher(User.Claims) || !uGroup.IsDev(User.Claims))
            {
                return RedirectToAction(nameof(Index));
            }
            
            if (id == null)
            {
                return NotFound();
            }

            var studentWorkshopEntry = await context.StudentWorkshopEntries
                .FirstOrDefaultAsync(m => m.Id == id);
            if (studentWorkshopEntry == null)
            {
                return NotFound();
            }

            return View(studentWorkshopEntry);
        }

        [HttpPost, ActionName("DeleteEntry")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteEntryConfirmed(int id)
        {
            // Disallow access for unauthenticated users and non-privileged users
            var uGroup = new UserGroup(context);
            if (User.Identity == null || !uGroup.IsTeacher(User.Claims) || !uGroup.IsDev(User.Claims))
            {
                return RedirectToAction(nameof(Index));
            }
            
            var studentWorkshopEntry = await context.StudentWorkshopEntries.FindAsync(id);
            if (studentWorkshopEntry != null)
            {
                var wsEntry = context.WorkshopEntries
                    .FirstOrDefault(w => w.Id == studentWorkshopEntry.WorkshopEntryId);
                wsEntry.RegisteredStudents--;
                context.StudentWorkshopEntries.Remove(studentWorkshopEntry);
            }

            await context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
    }
}