using Espit.Data;
using Espit.Models;
using Espit.Models.Entries;
using Espit.Models.Wrappers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Espit.Controllers;

public class StudentCoursesController(EspitContext context) : Controller
{
    // Used in the CoursesController Index method for better UX during registration
   
    public StudentCoursesWrapper GetStudentCoursesWrapper(string name)
    {
        //var studentEntries = context.StudentCourseEntries.ToListAsync().Result;
        var studentEntries = context.StudentCourseEntries.Where(e => e.Name == name).ToList();

        var studentWrapper = new StudentCoursesWrapper
        {
            Name = name,
            Courses = new Dictionary<Course, bool>()
        };

        foreach (var entry in studentEntries)
        {
            var course = context.Courses.FirstOrDefault(w => w.Id == entry.CourseId);
            if(course == null)
            {
                continue;
            }
            
            studentWrapper.Courses.Add(course, entry.Mandatory);
        }

        return studentWrapper;
    }
}