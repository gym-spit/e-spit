using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Espit.Data;
using Espit.Models;
using Espit.Models.Entries;
using Espit.Models.Wrappers;
using Espit.Services;

namespace Espit.Controllers;

public class WorkshopController(EspitContext context) : Controller
{
    // GET: Workshop
    public async Task<IActionResult> Index()
    {
        var registeredWorkshops = context.Workshops.ToListAsync().Result;
        List<WorkshopWrapper> wrapper = new List<WorkshopWrapper>();
        foreach (var workshop in registeredWorkshops)
        {
            var thisWorkshopWrapper = new WorkshopWrapper
            {
                Workshop = workshop,
                Enrollments = new Dictionary<int, int>()
            };

            var thisWorkshopEntries = context.WorkshopEntries.Where(entry => entry.WorkshopId == workshop.Id)
                .ToListAsync().Result;
            foreach (var entry in thisWorkshopEntries)
            {
                var registeredStudents = context.StudentWorkshopEntries.Count(e => e.WorkshopEntryId == entry.Id);
                thisWorkshopWrapper.Enrollments.Add(entry.BlockNumber, registeredStudents);
            }

            var uGroup = new UserGroup(context);
            if (uGroup.IsTeacher(User.Claims))
            {
                thisWorkshopWrapper.HasElevatedPrivileges =
                    thisWorkshopWrapper.HasElevatedPrivileges with { isTeacher = true };
            }
            else
            {
                thisWorkshopWrapper.ThisStudentWrapper =
                    new StudentController(context).GetStudentWorkshopWrapper(User.Claims
                        .FirstOrDefault(c => c.Type == "name")!.Value);
            }

            if (uGroup.IsDev(User.Claims))
            {
                thisWorkshopWrapper.HasElevatedPrivileges =
                    thisWorkshopWrapper.HasElevatedPrivileges with { isDev = true };
            }

            wrapper.Add(thisWorkshopWrapper);
        }

        return View(wrapper);
    }

    public async Task<IActionResult> Entries()
    {
        // Disallow access for unauthenticated users and non-privileged users
        var uGroup = new UserGroup(context);
        if (User.Identity == null || (!uGroup.IsTeacher(User.Claims) && !uGroup.IsDev(User.Claims)))
        {
            return RedirectToAction(nameof(Index));
        }

        return View(await context.WorkshopEntries.ToListAsync());
    }

    public async Task<IActionResult> Register(int workshopId, int blockNumber)
    {
        // Disallow registration for teachers and unauthenticated users
        var uGroup = new UserGroup(context);
        if (User.Identity == null || uGroup.IsTeacher(User.Claims))
        {
            return RedirectToAction(nameof(Index));
        }

        // Check if the currently registered workshop exists
        var workshop = await context.Workshops.FindAsync(workshopId);
        if (workshop == null)
        {
            return RedirectToAction(nameof(Index));
        }

        List<int> workshopEntryIds = new List<int>();
        for (var i = 0; i < workshop.Length; i++)
        {
            int workshopEntryId = context.WorkshopEntries
                .Where(entry => entry.WorkshopId == workshopId && entry.BlockNumber == blockNumber + i)
                .Select(entry => entry.Id)
                .FirstOrDefault();
            workshopEntryIds.Add(workshopEntryId);
        }

        var thisUserName = User.Claims.FirstOrDefault(c => c.Type == "name")!.Value;
        var thisStudentEntries = context.StudentWorkshopEntries.Where(e => e.Name == thisUserName);

        // Check if user is already registered for this workshop
        foreach (var studentEntry in thisStudentEntries)
        {
            //var wsEntry = await context.WorkshopEntries.FirstOrDefaultAsync(e => e.WorkshopId == entry.WorkshopEntryId);
            var wsEntry = await context.WorkshopEntries.FindAsync(studentEntry.WorkshopEntryId);
            if (wsEntry == null)
            {
                return RedirectToAction(nameof(Index));
            }

            for (var i = 0; i < workshop.Length; i++)
            {
                if (wsEntry.BlockNumber == blockNumber + i || wsEntry.WorkshopId == workshopId)
                {
                    return RedirectToAction(nameof(Index));
                }
            }
        }

        // Check if workshop entries exists and if there is still capacity
        List<WorkshopEntry> workshopEntries = new List<WorkshopEntry>();
        foreach (var workshopEntryId in workshopEntryIds)
        {
            var workshopEntry = await context.WorkshopEntries.FindAsync(workshopEntryId);
            // Check if workshop entries exists
            if (workshopEntry == null)
            {
                return RedirectToAction(nameof(Index));
            }

            var registeredStudents = context.StudentWorkshopEntries
                .Count(e => e.WorkshopEntryId == workshopEntryId);
            
            // Check if there is still capacity
            if (registeredStudents >= workshop.Capacity)
            {
                return RedirectToAction(nameof(Index));
            }

            workshopEntries.Add(workshopEntry);
        }


        foreach (var workshopEntryId in workshopEntryIds)
        {
            context.Add(new StudentWorkshopEntry
            {
                Name = User.Claims.FirstOrDefault(c => c.Type == "name")?.Value,
                Class = new UserGroup(context).GetUserGroup(User.Claims),
                WorkshopEntryId = workshopEntryId
            });

            await context.SaveChangesAsync();
        }

        foreach (var workshopEntry in workshopEntries)
        {
            workshopEntry.RegisteredStudents += 1;
            context.Update(workshopEntry);
            await context.SaveChangesAsync();
        }

        return RedirectToAction(nameof(Index));
    }

    public async Task<IActionResult> Deregister(int workshopId, int blockNumber)
    {
        // Disallow deregistration for teachers and unauthenticated users
        var uGroup = new UserGroup(context);
        if (User.Identity == null || uGroup.IsTeacher(User.Claims))
        {
            return RedirectToAction(nameof(Index));
        }

        // Check if the currently registered workshop exists
        var workshop = await context.Workshops.FindAsync(workshopId);
        if (workshop == null)
        {
            return RedirectToAction(nameof(Index));
        }

        List<int> workshopEntryIds = new List<int>();
        for (var i = 0; i < workshop.Length; i++)
        {
            int workshopEntryId = context.WorkshopEntries
                .Where(entry => entry.WorkshopId == workshopId && entry.BlockNumber == blockNumber+i)
                .Select(entry => entry.Id)
                .FirstOrDefault();
            workshopEntryIds.Add(workshopEntryId);
        }

        var thisUserName = User.Claims.FirstOrDefault(c => c.Type == "name")!.Value;
        var thisStudentEntries = context.StudentWorkshopEntries
            .Where(e => e.Name == thisUserName);

        // Check if workshop entry exists
        foreach (var workshopEntryId in workshopEntryIds)
        {
            var workshopEntry = await context.WorkshopEntries.FindAsync(workshopEntryId);
            if (workshopEntry == null)
            {
                return RedirectToAction(nameof(Index));
            }

            // Check if user is already registered for this workshop
            foreach (var entry in thisStudentEntries)
            {
                var wsEntry = context.WorkshopEntries.FindAsync(entry.WorkshopEntryId).Result;

                if (wsEntry == null)
                {
                    return RedirectToAction(nameof(Index));
                }

                // Remove the entry if it exists
                /*if (wsEntry.BlockNumber == blockNumber && wsEntry.WorkshopId == workshopId)*/
                if (wsEntry.WorkshopId == workshopId)
                {

                    /*
                    foreach (var entryId in workshopEntryIds)
                    {
                        context.Remove(context.StudentWorkshopEntries
                            .FirstOrDefault(e => e.WorkshopEntryId == entryId && e.Name == thisUserName));
                        workshopEntry.RegisteredStudents -= 1;
                        context.Update(workshopEntry);
                        await context.SaveChangesAsync();
                    }*/
                    
                    context.Remove(entry);
                    //await context.SaveChangesAsync();
                    //workshopEntry.RegisteredStudents -= 1;
                    context.Update(workshopEntry);
                    await context.SaveChangesAsync();
                    //return RedirectToAction(nameof(Index));
                }
            }
        }

        return RedirectToAction(nameof(Index));
    }


    // GET: Workshop/Details/5
    public async Task<IActionResult> Details(int? id)
    {
        if (id == null)
        {
            return NotFound();
        }

        var workshop = await context.Workshops
            .FirstOrDefaultAsync(m => m.Id == id);
        if (workshop == null)
        {
            return NotFound();
        }

        return View(workshop);
    }

    // GET: Workshop/Create
    public IActionResult Create()
    {
        // Disallow access for unauthenticated users and non-privileged users
        var uGroup = new UserGroup(context);
        if (User.Identity == null || (!uGroup.IsTeacher(User.Claims) && !uGroup.IsDev(User.Claims)))
        {
            return RedirectToAction(nameof(Index));
        }

        return View();
    }

    // POST: Workshop/Create
    // To protect from overposting attacks, enable the specific properties you want to bind to.
    // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Create([Bind("Id,Title,HostName,Annotation,Capacity,Length,Start")] Workshop workshop)
    {
        // Disallow access for unauthenticated users and non-privileged users
        var uGroup = new UserGroup(context);
        if (User.Identity == null || (!uGroup.IsTeacher(User.Claims) && !uGroup.IsDev(User.Claims)))
        {
            return RedirectToAction(nameof(Index));
        }

        if (ModelState.IsValid)
        {
            if(workshop.Length == null)
                workshop.Length = 1;
            
            if(workshop.Start == null)
                workshop.Start = 1;
            
            context.Add(workshop);
            await context.SaveChangesAsync();
            
            for (var i = 1; i <= 3; i++)
            {
                context.Add(new WorkshopEntry
                {
                    WorkshopId = workshop.Id,
                    BlockNumber = i,
                    RegisteredStudents = 0
                });

                await context.SaveChangesAsync();
            }

            await context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        return View(workshop);
    }

    // GET: Workshop/Edit/5
    public async Task<IActionResult> Edit(int? id)
    {
        // Disallow access for unauthenticated users and non-privileged users
        var uGroup = new UserGroup(context);
        if (User.Identity == null || (!uGroup.IsTeacher(User.Claims) && !uGroup.IsDev(User.Claims)))
        {
            return RedirectToAction(nameof(Index));
        }

        if (id == null)
        {
            return NotFound();
        }

        var workshop = await context.Workshops.FindAsync(id);
        if (workshop == null)
        {
            return NotFound();
        }

        return View(workshop);
    }

    // POST: Workshop/Edit/5
    // To protect from overposting attacks, enable the specific properties you want to bind to.
    // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Edit(int id, [Bind("Id,Title,HostName,Annotation,Capacity")] Workshop workshop)
    {
        // Disallow access for unauthenticated users and non-privileged users
        var uGroup = new UserGroup(context);
        if (User.Identity == null || (!uGroup.IsTeacher(User.Claims) && !uGroup.IsDev(User.Claims)))
        {
            return RedirectToAction(nameof(Index));
        }

        if (id != workshop.Id)
        {
            return NotFound();
        }

        if (ModelState.IsValid)
        {
            try
            {
                context.Update(workshop);
                await context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!WorkshopExists(workshop.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToAction(nameof(Index));
        }

        return View(workshop);
    }

    public async Task<IActionResult> Delete(int? id)
    {
        // Disallow access for unauthenticated users and non-privileged users
        var uGroup = new UserGroup(context);
        if (User.Identity == null || (!uGroup.IsTeacher(User.Claims) && !uGroup.IsDev(User.Claims)))
        {
            return RedirectToAction(nameof(Index));
        }

        if (id == null)
        {
            return NotFound();
        }

        var workshop = await context.Workshops
            .FirstOrDefaultAsync(m => m.Id == id);
        if (workshop == null)
        {
            return NotFound();
        }

        return View(workshop);
    }

    [HttpPost, ActionName("Delete")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> DeleteConfirmed(int id)
    {
        // Disallow access for unauthenticated users and non-privileged users
        var uGroup = new UserGroup(context);
        if (User.Identity == null || (!uGroup.IsTeacher(User.Claims) && !uGroup.IsDev(User.Claims)))
        {
            return RedirectToAction(nameof(Index));
        }

        var workshop = await context.Workshops.FindAsync(id);
        if (workshop != null)
        {
            context.Workshops.Remove(workshop);
        }

        await context.SaveChangesAsync();
        return RedirectToAction(nameof(Index));
    }

    private bool WorkshopExists(int id)
    {
        return context.Workshops.Any(e => e.Id == id);
    }

    public async Task<IActionResult> DeleteEntry(int? id)
    {
        // Disallow access for unauthenticated users and non-privileged users
        var uGroup = new UserGroup(context);
        if (User.Identity == null || (!uGroup.IsTeacher(User.Claims) && !uGroup.IsDev(User.Claims)))
        {
            return RedirectToAction(nameof(Index));
        }

        if (id == null)
        {
            return NotFound();
        }

        var workshopEntry = await context.WorkshopEntries
            .FirstOrDefaultAsync(m => m.Id == id);
        if (workshopEntry == null)
        {
            return NotFound();
        }

        return View(workshopEntry);
    }

    [HttpPost, ActionName("DeleteEntry")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> DeleteEntryConfirmed(int id)
    {
        // Disallow access for unauthenticated users and non-privileged users
        var uGroup = new UserGroup(context);
        if (User.Identity == null || (!uGroup.IsTeacher(User.Claims) && !uGroup.IsDev(User.Claims)))
        {
            return RedirectToAction(nameof(Index));
        }

        var workshopEntry = await context.WorkshopEntries.FindAsync(id);
        if (workshopEntry != null)
        {
            context.WorkshopEntries.Remove(workshopEntry);
        }

        await context.SaveChangesAsync();
        return RedirectToAction(nameof(Entries));
    }
}