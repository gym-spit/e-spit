using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;

namespace Espit.Data
{
    public class EspitContext(DbContextOptions<EspitContext> options) : DbContext(options)
    {
        public DbSet<Espit.Models.Event> Events { get; set; } = default!;
        public DbSet<Espit.Models.Workshop> Workshops { get; set; } = default!;
        public DbSet<Espit.Models.Entries.WorkshopEntry> WorkshopEntries { get; set; } = default!;
        public DbSet<Espit.Models.Entries.StudentWorkshopEntry> StudentWorkshopEntries { set; get; } = default!;
        public DbSet<Espit.Models.Course> Courses { get; set; } = default!;
        public DbSet<Espit.Models.Entries.StudentCourseEntry> StudentCourseEntries { get; set; } = default!;
        public DbSet<Espit.Models.Club> Clubs { get; set; } = default!;
        public DbSet<Espit.Models.Entries.StudentClubEntry> StudentClubEntries { get; set; } = default!;
        public DbSet<Espit.Models.ActiveDirectoryGroup> ActiveDirectoryGroups { get; set; } = default!;
        public DbSet<Espit.Models.ActiveDirectoryUser> ActiveDirectoryUsers { get; set; } = default!;
    }
}