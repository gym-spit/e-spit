using System.ComponentModel.DataAnnotations;

namespace Espit.Models;

public class ActiveDirectoryGroup
{
    public int Id { get; set; }
    
    [Display(Name = "ID skupiny")]
    public string GroupIdentifier { get; set; }
    
    [Display(Name = "Název skupiny")]
    public string? GroupName { get; set; }
}