using System.ComponentModel.DataAnnotations;

namespace Espit.Models;

public class ActiveDirectoryUser
{
    public int Id { get; set; }

    [Display(Name = "Jméno uživatele")]
    public string PreferredUsername { get; set; }
}