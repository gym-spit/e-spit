using System.ComponentModel.DataAnnotations;

namespace Espit.Models;

public class Course
{
    public int Id { get; set; }
    
    [Display(Name = "Předmět")]
    public string? Title { get; set; }
   
    [Display(Name = "Budoucí 2.V")]
    public bool EligibleClassSecunda { get; set; }
    
    [Display(Name = "Budoucí 3.V")]
    public bool EligibleClassTercie { get; set; }
    
    [Display(Name = "Budoucí 4.V")]
    public bool EligibleClassQuarta { get; set; }
    
    [Display(Name = "Budoucí 5.V")]
    public bool EligibleClassQuinta { get; set; }
    
    [Display(Name = "Budoucí 6.V")]
    public bool EligibleClassSexta { get; set; }
    
    [Display(Name = "Budoucí 7.V")]
    public bool EligibleClassSeptima { get; set; }
    
    [Display(Name = "Budoucí 8.V")]
    public bool EligibleClassOctava { get; set; }

    [Display(Name = "Budoucí 2.B")]
    public bool EligibleClassSophomore { get; set; }
    
    [Display(Name = "Budoucí 3.B")]
    public bool EligibleClassJunior { get; set; }
    
    [Display(Name = "Budoucí 4.B")]
    public bool EligibleClassSenior { get; set; }
    
    public bool MandatoryOnly { get; set; }
    
}