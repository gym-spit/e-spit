namespace Espit.Models.Entries;

public class StudentClubEntry
{
    public int Id { get; set; }
    
    public string Name { get; set; } = "";
    public string? Class { get; set; } = "";
    public int ClubId { get; set; }
}