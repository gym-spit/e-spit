namespace Espit.Models.Entries;

public class StudentCourseEntry
{
    public int Id { get; set; }
    
    public string Name { get; set; } = "";
    public string? Class { get; set; } = "";
    public int CourseId { get; set; }
    public bool Mandatory { get; set; }
}