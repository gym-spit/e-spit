using System.ComponentModel.DataAnnotations;

namespace Espit.Models.Entries;

public class StudentWorkshopEntry
{
    public int Id { get; set; }
    public string Name { get; set; } = "";
    public string? Class { get; set; } = "";
    public int WorkshopEntryId { get; set; }
}