namespace Espit.Models.Entries;

public class WorkshopEntry
{
    public int Id { get; set; }
    
    public int WorkshopId { get; set; }
    
    public int BlockNumber { get; set; }
    
    public int RegisteredStudents { get; set; }
}