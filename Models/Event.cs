using System.ComponentModel.DataAnnotations;

namespace Espit.Models;

public class Event
{
    public int Id { get; set; }

    [Display(Name = "Název akce")]
    public string? Title { get; set; }
    
    [Display(Name = "Vlastník")]
    public string? Owner { get; set; }
    
    [Display(Name = "Týká se tříd")]
    public string? EligibleClasses { get; set; }
}