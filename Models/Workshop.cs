using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Espit.Models;
public class Workshop
{
    public int Id { get; set; }
    
    [Display(Name = "Název dílny")]
    public string? Title { get; set; }
    
    [Display(Name = "Host")]
    public string? HostName { get; set; }
    
    [Display(Name = "Anotace")]
    public string? Annotation { get; set; }
    
    [Display(Name = "Kapacita")]
    public int? Capacity { get; set; }

    [Display(Name = "Délka (pro víceblokové dílny)")]
    [DefaultValue(1)]
    public int? Length { get; set; }

    [Display(Name = "Začátek (pro víceblokové dílny)")]
    [DefaultValue(1)]
    public int? Start { get; set; }
}