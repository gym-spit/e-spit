using System.ComponentModel.DataAnnotations;

namespace Espit.Models.Wrappers;

public class ClubsWrapper
{
    public Club Club { get; set; } = new();

    [Display(Name = "Počet zapsaných studentů")]
    public int EnrolledStudents { get; set; }

    public StudentClubsWrapper StudentWrapper { get; set; } = new();
    
    public (bool isTeacher, bool isDev) HasElevatedPrivileges { get; set; } = (false, false);
}