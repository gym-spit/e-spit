using System.ComponentModel.DataAnnotations;

namespace Espit.Models.Wrappers;

public class CoursesWrapper
{
    public Course Course { get; set; } = new();

    [Display(Name = "Počet zapsaných studentů")]
    public int EnrolledStudents { get; set; }

    public StudentCoursesWrapper StudentWrapper { get; set; } = new();
    
    public (bool isTeacher, bool isDev) HasElevatedPrivileges { get; set; } = (false, false);
}