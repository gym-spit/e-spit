using System.ComponentModel.DataAnnotations;

namespace Espit.Models.Wrappers;

public class StudentClubsWrapper
{
    [Display(Name = "Jméno")] public string? Name { get; set; }

    [Display(Name = "Třída")] public string? Class { get; set; }
    public List<Club> Clubs { get; set; } = new();
}