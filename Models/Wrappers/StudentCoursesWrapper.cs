using System.ComponentModel.DataAnnotations;
using Espit.Models.Entries;

namespace Espit.Models.Wrappers;

public class StudentCoursesWrapper
{
    [Display(Name = "Jméno")] public string? Name { get; set; }

    [Display(Name = "Třída")] public string? Class { get; set; }
    public Dictionary<Course, bool> Courses { get; set; } = new();
    public int ForcedCourseCount { get; set; } = 0;
}