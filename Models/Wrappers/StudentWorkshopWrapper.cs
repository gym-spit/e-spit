using System.ComponentModel.DataAnnotations;

namespace Espit.Models.Wrappers;

public class StudentWorkshopWrapper
{
    [Display(Name = "Jméno")] public string? Name { get; set; }

    [Display(Name = "Třída")] public string? Class { get; set; }

    // BlockNumber, Workshop
    public Dictionary<int, Workshop> Workshops { get; set; } = new();
}