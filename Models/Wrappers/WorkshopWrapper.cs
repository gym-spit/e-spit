using System.ComponentModel.DataAnnotations;

namespace Espit.Models.Wrappers;

public class WorkshopWrapper
{
    public Workshop Workshop { get; set; } = new();

    // The first int is the number of time slot, the second int is the number of participants registered for said time slot.
    public Dictionary<int, int> Enrollments { get; set; } = new();

    // Wrapper to display/hide the (de)register button according to the user's enrolled workshops
    public StudentWorkshopWrapper ThisStudentWrapper { get; set; } = new();
    
    public (bool isTeacher, bool isDev) HasElevatedPrivileges { get; set; } = (false, false);
}