# E-špit

Elektronický informační systém gymnázia Špitálská

Web: ![](https://status.toohka.org/api/badge/7/status) Server: ![](https://status.toohka.org/api/badge/8/status) [status page](https://status.toohka.org/status/espit)

## Vývoj

### Dependencies

Balíčky, které systém používá, nainstalujte následujícími příkazy:
```
# Balíčky ASP.NET Core
dotnet add package Microsoft.EntityFrameworkCore.Design
dotnet add package Microsoft.EntityFrameworkCore.SQLite
dotnet add package Microsoft.VisualStudio.Web.CodeGeneration.Design
dotnet add package Microsoft.EntityFrameworkCore.SqlServer
dotnet add package Microsoft.EntityFrameworkCore.Tools

# Balíčky pro autentizaci před Microsoft EntraID
dotnet add package Microsoft.Identity.Web.UI
dotnet add package Microsoft.Identity.Web.Diagnostics
dotnet add package Microsoft.Identity.Web.DownstreamApi 
```

### Inicializace databáze

Projekt během vývoje běží na SQLite. Je třeba si lokálně inicializovat databázi,
a to následuícím příkazem:

```
dotnet ef migrations add InitialCreate
dotnet ef database update
```

### Appsettings

Soubor `appsettings.json` vzhledem ke své povaze obsahuje placeholdery.
Pro členy vývojového týmu jsou data dostupná [zde](https://gitlab.com/gym-spit/secrets)

### Přidávání modelů a controllerů

Po vytvoření nového modelu lze jeho controller vytvořit následujícím příkazem:

```
dotnet aspnet-codegenerator controller -name <Model>sController -m <Model> -dc Espit.Data.EspitContex --relativeFolderPath Controllers --useDefaultLayout --referenceScriptLibraries --databaseProvider sqlite
```
