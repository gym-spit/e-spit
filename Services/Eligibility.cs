using Espit.Models;

namespace Espit.Services;

/** <summary>
 * Determines whether user is eligible for registering certain course or club.
 * Functions use current user group and course/club to determine eligibility.
 * Course/club eligibility holds classes in the next school year.
 * Class is used in CoursesController and StudentCoursesWrapper.
 * </summary>
 * <returns>bool</returns>
 */

public class Eligibility
{
    public bool IsEligibleFor(string userGroup, Course course)
    {
        var eligibleClasses = CourseEligibleClasses(course);
        return eligibleClasses.Contains(userGroup);
    }

    public bool IsEligibleFor(string userGroup, Club club)
    {
        var eligibleClasses = ClubEligibleClasses(club);
        return eligibleClasses.Contains(userGroup);
    }

    private List<string> CourseEligibleClasses(Course course)
    {
        var eligibleClasses = new List<string>();
        if (course.EligibleClassSecunda)
        {
            eligibleClasses.Add("1.V");
        }

        if (course.EligibleClassTercie)
        {
            eligibleClasses.Add("2.V");
        }

        if (course.EligibleClassQuarta)
        {
            eligibleClasses.Add("3.V");
        }

        if (course.EligibleClassQuinta)
        {
            eligibleClasses.Add("4.V");
        }

        if (course.EligibleClassSexta)
        {
            eligibleClasses.Add("5.V");
        }

        if (course.EligibleClassSeptima)
        {
            eligibleClasses.Add("6.V");
        }

        if (course.EligibleClassOctava)
        {
            eligibleClasses.Add("7.V");
        }

        if (course.EligibleClassSophomore)
        {
            eligibleClasses.Add("1.B");
        }

        if (course.EligibleClassJunior)
        {
            eligibleClasses.Add("2.B");
        }

        if (course.EligibleClassSenior)
        {
            eligibleClasses.Add("3.B");
        }

        return eligibleClasses;
    }

    private List<string> ClubEligibleClasses(Club club)
    {
        var eligibleClasses = new List<string>();
        if (club.EligibleClassSecunda)
        {
            eligibleClasses.Add("1.V");
        }

        if (club.EligibleClassTercie)
        {
            eligibleClasses.Add("2.V");
        }

        if (club.EligibleClassQuarta)
        {
            eligibleClasses.Add("3.V");
        }

        if (club.EligibleClassQuinta)
        {
            eligibleClasses.Add("4.V");
        }

        if (club.EligibleClassSexta)
        {
            eligibleClasses.Add("5.V");
        }

        if (club.EligibleClassSeptima)
        {
            eligibleClasses.Add("6.V");
        }

        if (club.EligibleClassOctava)
        {
            eligibleClasses.Add("7.V");
        }

        if (club.EligibleClassSophomore)
        {
            eligibleClasses.Add("1.B");
        }

        if (club.EligibleClassJunior)
        {
            eligibleClasses.Add("2.B");
        }

        if (club.EligibleClassSenior)
        {
            eligibleClasses.Add("3.B");
        }

        return eligibleClasses;
    }
}