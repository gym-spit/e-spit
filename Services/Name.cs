namespace Espit.Services;

public class Name
{
    public string SwapFirstAndLastName(string fullName)
    {
        // Split the full name into individual names
        string[] names = fullName.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

        // If there's only one name or no last name, return the original name
        if (names.Length <= 1)
            return fullName;

        // Concatenate the last name
        string lastName = names[names.Length - 1];

        // Concatenate the first names
        string firstName = string.Join(" ", names, 0, names.Length - 1);

        // Concatenate the swapped names
        return $"{lastName} {firstName}";
    }
}