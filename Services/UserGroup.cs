using System.Security.Claims;
using Espit.Data;

namespace Espit.Services;
public class UserGroup(EspitContext context)
{
    // TODO Metodu je potřeba předělat, ideálně odstranit claimy na nepotřebné skupiny
    public string GetUserGroup(IEnumerable<Claim> claims)
    {
        var thisUserGroups = claims.Where(c => c.Type == "groups").Select(c => c.Value).ToList();
        var groupsAvailable = context.ActiveDirectoryGroups.Select(g => g.GroupIdentifier).ToList();

        string searchedGroup = "";

        foreach (var group in thisUserGroups)
        {
            var groupFound = groupsAvailable.FirstOrDefault(g => g == group);
            if (groupFound != null)
            {
                searchedGroup = groupFound;
                break;
            }
        }

        var thisUserClass = context.ActiveDirectoryGroups
            .FirstOrDefault(g => g.GroupIdentifier == searchedGroup)?.GroupName;
        
        return thisUserClass ?? "NIL";
    }

    public bool IsTeacher(IEnumerable<Claim> claims)
    {
        return GetUserGroup(claims) == "Profesoři";
    }
    
    public bool IsDev(IEnumerable<Claim> claims)
    {
        var pun = claims.FirstOrDefault(c => c.Type == "preferred_username")?.Value;
        var possibleDevUser = context.ActiveDirectoryUsers.FirstOrDefault(u => u.PreferredUsername == pun);
        
        return possibleDevUser != null;
    }
}